import ConfigContext from '/context/config';
import configApiSettings from '/context/config/api';
import Discovery from '/components/discovery';
import Filter from '/components/filter';
import FilterContext from '/components/filter/context';
import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM  from 'react-dom';
import Search from '/components/search';
import withSuspense from '/hoc/suspense';

function Home({ resource: configApi }) {
  const config = configApi.read();

  const [filter, setFilter] = React.useState({ stars: undefined });

  const filterProvider = { filter, setFilter };

  return (
    <ConfigContext.Provider value={config}>
      <FilterContext.Provider value={filterProvider}>
        <Filter />

        <Search />

        <Discovery />
      </FilterContext.Provider>
    </ConfigContext.Provider>
  );
}

Home.propTypes = {
  resource: PropTypes.object,
};

const App = withSuspense(Home, configApiSettings);

const rootNode = document.getElementById('app');
ReactDOM.unstable_createRoot(rootNode).render(<App />);
