import createResource from '/api';
import PropTypes from 'prop-types';
import React from 'react';

function withSuspense(WrappedComponent, resourceSettings) {
  const resource = resourceSettings && createResource(resourceSettings);

  function SuspenseWrapper({ resource: ownResource }) {
    return (
      <React.Suspense fallback={<h1>...Loading</h1>}>
        <WrappedComponent resource={resource || ownResource} />
      </React.Suspense>
    );
  }

  SuspenseWrapper.propTypes = {
    resource: PropTypes.object
  };

  return SuspenseWrapper;
}

export default withSuspense;
