import axios from 'axios';
import humps from 'lodash-humps';
import isArray from 'lodash/isArray';
import mergeWith from 'lodash/mergeWith';
import qs from 'qs';

const api_key = process.env.API_KEY;

const DEFAULTS = {
  method: 'GET',
  params: { api_key },
  paramsSerializer: qs.stringify,
  transformResponse: [
    ...axios.defaults.transformResponse,
    humps
  ],
};

function customizer(target, source) {
  return isArray(target)
    ? target.concat(source)
    : undefined;
}

function api(settings) {
  return axios(mergeWith({}, DEFAULTS, settings, customizer));
}

export default function createResource(settings) {
  let status = 'loading';
  let result;
  let suspender = api(settings).then(
    ({ data }) => {
      status = 'success';
      result = data;
    },
    error => {
      status = 'error';
      result = error;
    }
  );

  return {
    read() {
      if (status === 'loading') {
        throw suspender;
      } else if (status === 'error') {
        throw result;
      } else if (status === 'success') {
        return result;
      }
    }
  };
}
