export const API_BASE= 'https://api.themoviedb.org/3';

export default {
  DISCOVER: `${API_BASE}/discover/movie`,
  CONFIGURATION: `${API_BASE}/configuration`,
  SEARCH: `${API_BASE}/search/movie`
};
