import paths from '/api/paths';
import DiscoverResults from './class';

export default {
  url: paths.DISCOVER,
  params: {
    language: 'en-US',
    sortBy: 'popularity.desc',
    includeAdult: false,
    includeVideo: false,
    page: 1,
  },
  transformResponse: data => new DiscoverResults(data)
};
