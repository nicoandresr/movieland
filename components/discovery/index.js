import discoverApiSettings from './api';
import filterContext from '/components/filter/context';
import Poster from '/components/poster';
import PropTypes from  'prop-types';
import React from 'react';
import withSuspense from '/hoc/suspense';

function Movies({ resource: discoveryApi }) {

  const { results } = discoveryApi.read();
  const { filter: { stars } } = React.useContext(filterContext);

  function filterByStars(item) {
    return !stars || item.voteAverage >= stars;
  }

  return (
    <ul className="flex flex-wrap container mx-auto justify-center">
      {results.filter(filterByStars).map(title => <Poster title={title} key={title.posterPath} />)}
    </ul>
  );
}

Movies.propTypes = {
  resource: PropTypes.object
};

export default withSuspense(Movies, discoverApiSettings);
