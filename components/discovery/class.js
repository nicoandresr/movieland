class DiscoveryResults {
  constructor(rawData) {
    const { results, ...rest } = rawData;

    return {
      results: results.sort((a, b) => a.popularity - b.popularity ? -1 : 1),
      ...rest
    };
  }
}

export default DiscoveryResults;
