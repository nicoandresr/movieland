import createResource from '/api';
import paths from '/api/paths';
import Search from './class';

function Api(query) {
  if (!query || query.length <= 3)
    return { read: () => ({
      results: []
    })};

  return createResource({
    url: paths.SEARCH,
    params: {
      query,
      include_adult: false
    },
    transformResponse: data => new Search(data)
  });
}

export default Api;
