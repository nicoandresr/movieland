class Search {
  constructor(rawData) {
    const { results } = rawData;
    this.results = results
      .filter(title => !!title.posterPath)
      .sort((a, b) => b.popularity - a.popularity)
  }
}

export default Search;
