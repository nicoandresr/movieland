import searchApi from './api';
import React from 'react';
import Results from './results';

function Search() {
  const [api, setApi] = React.useState(searchApi());

  function onSearch(event) {
    event.preventDefault();
    const { value: queryString } = event.target;
    
    setApi(searchApi(queryString));
  }

  return (
    <section id="search">
      <input type="text" onChange={onSearch} />

      <Results resource={api} />
    </section>
  );
}

export default Search;
