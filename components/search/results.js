import Poster from '/components/poster';
import PropTypes from 'prop-types';
import React from 'react';
import withSuspense from '/hoc/suspense';

function Results({ resource: searchApi }) {
  const { results } = searchApi.read();

  return (
    <ul className="flex flex-wrap container mx-auto justify-center">
      {results.map(title => <Poster title={title} key={title.posterPath} />)}
    </ul>
  );
}

Results.propTypes = {
  resource: PropTypes.object,
};

export default withSuspense(Results);
