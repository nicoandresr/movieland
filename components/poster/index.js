import ConfigContext from '/context/config';
import PropTypes from  'prop-types';
import React from 'react';

const propTypes = {
  title: PropTypes.shape({
    overview: PropTypes.string,
    posterPath: PropTypes.string,
    title: PropTypes.string
  })
};

function Poster({ title }) {
  const { posterBaseUrl } = React.useContext(ConfigContext);
  const [show, setShow] = React.useState(false);

  function toggleShow() {
    setShow(!show);
  }

  return (
    <li className="m-2 cursor-pointer flex">
      <img src={`${posterBaseUrl}${title.posterPath}`}  onClick={toggleShow} />

      {show && (
        <div className="fixed w-100 h-100 flex bg-black bg-opacity-75 inset-0" onClick={toggleShow}>
          <div className="m-auto p-8 max-w-5xl bg-gray-900 flex">
            <img className="w-64 mr-8" src={`${posterBaseUrl}${title.posterPath}`} />

            <div className="max-w-xl">
              <h1 className="text-5xl leading-tight font-bold font-serif mb-4 text-gray-400 tracking-wide">{title.title}</h1>
              <p className="text-xl text-white">{title.overview}</p>
            </div>
          </div>
        </div>
      )}
    </li>
  );
}

Poster.propTypes = propTypes;

export default Poster;
