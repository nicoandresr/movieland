import context from './context';
import React from 'react';

function Filter() {

  const { setFilter } = React.useContext(context);

  function onChangeHandler({ target }) {
    setFilter({ stars: target.value });
  }

  return (
    <>
      <input type="radio" name="stars" value={0} id="1-star" onChange={onChangeHandler} />
      <label htmlFor="1-star">1 star</label>

      <input type="radio" name="stars" value={2} id="2-star" onChange={onChangeHandler} />
      <label htmlFor="2-star">2 star</label>

      <input type="radio" name="stars" value={4} id="3-star" onChange={onChangeHandler} />
      <label htmlFor="3-star">3 star</label>

      <input type="radio" name="stars" value={6} id="4-star" onChange={onChangeHandler} />
      <label htmlFor="4-star">4 star</label>

      <input type="radio" name="stars" value={8} id="5-star" onChange={onChangeHandler} />
      <label htmlFor="5-star">5 star</label>
    </>
  );
}

export default Filter;
