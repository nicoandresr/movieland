import React from 'react';

const starsContext = React.createContext({ filter: undefined, setFilter: () => {} });

export default starsContext;
