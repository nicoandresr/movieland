class Config {
  constructor(rawData) {
    const { baseUrl, posterSizes } = rawData.images;

    this.baseUrl = baseUrl;
    this.posterSizes = posterSizes;
  }

  get posterBaseUrl() {
    if (this.posterSizes.some(size => size === 'w185')) {
      return `${this.baseUrl}w185`;
    }

    console.trace('Poster size w185 is not available.');
    return `${this.baseUrl}original`;
  }
}

export default Config;
