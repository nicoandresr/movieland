import Config from './class';
import paths from '/api/paths';

const apiSettings = {
  url: paths.CONFIGURATION,
  transformResponse: data => new Config(data)
};

export default apiSettings;
